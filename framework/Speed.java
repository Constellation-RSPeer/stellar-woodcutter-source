package StellarWoodcutter.framework;

public enum Speed {
    AFK,
    SLOW,
    MEDIUM,
    NORMAL,
    FAST;

    @Override
    public String toString() {
        return this.name().toLowerCase().trim().replace("_", " ");
    }
}
