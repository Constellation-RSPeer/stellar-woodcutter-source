package StellarWoodcutter.framework;

public interface Task {
    boolean trigger();
    int execute();
}
