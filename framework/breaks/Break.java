package StellarWoodcutter.framework.breaks;

import StellarWoodcutter.framework.Constellation;
import StellarWoodcutter.res.DataStore;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptBlockingEvent;
import org.rspeer.script.events.LoginScreen;
import org.rspeer.ui.Log;

import java.util.concurrent.TimeUnit;

public class Break {

    //Time in ms
    private long executeTime;

    //Break settings
    private long startInterval;
    private long endInterval;
    private long deviation;
    private long duration;
    private long adjustedDuration;

    //running time
    private long elapsedTime;

    //Script stop watch
    private StopWatch stopWatch;

    //minute in ms
    private final long MINUTE = 60000;

    //context
    private Script ctx;

    public Break(Script ctx, StopWatch stopWatch, long startTime, long endInterval, long duration, long deviation){
        this.ctx = ctx;
        this.stopWatch = stopWatch;
        this.startInterval = startTime * MINUTE;
        this.endInterval = endInterval * MINUTE;
        this.duration = duration * MINUTE;
        this.deviation = deviation * MINUTE;
        generateBreak();
    }

    public void execute(){
        Constellation.logout();
        Time.sleep(1000,2000);
        if(!Game.isLoggedIn()) {
            DataStore.setCurrentTree(null);
            DataStore.setNextTree(null);

            ScriptBlockingEvent ls = new LoginScreen(ctx);
            ctx.removeBlockingEvent(ls.getClass());

            Log.info("Break started!");
            elapsedTime = System.currentTimeMillis();

            while(getElapsedTime() < getAdjustedDuration()) {
                Time.sleep(1000);
                if(ctx.isStopping()) break;
                long remaining = getAdjustedDuration() - getElapsedTime();
                long mins = TimeUnit.MILLISECONDS.toMinutes(remaining);
                long secs = TimeUnit.MILLISECONDS.toSeconds(remaining);
                Log.info("" + mins + "m "
                        + (secs - (mins*60))
                        + "s remaining");
            }

            if((getElapsedTime() > getAdjustedDuration()) || ctx.isStopping()){
                generateBreak();
                Log.info("Break finished");
                ctx.addBlockingEvent(ls);
            }
        }

    }

    private void generateBreak(){
        long rand = Random.nextLong(startInterval, endInterval);
        long st = stopWatch.getElapsed().toMillis() + rand;
        long min = getDuration() - getDeviation();
        long max = getDuration() + getDeviation();
        long dur = Random.nextLong(min, max);
        Log.info("Break Generated -> startTime: " +  formatTime(st) + " duration: " + formatTime(dur));
        setExecuteTime(st);
        setAdjustedDuration(dur);
    }

    private long getElapsedTime() { return System.currentTimeMillis() - elapsedTime; }

    long getDuration() { return duration; }

    private void setExecuteTime(long executeTime){ this.executeTime = executeTime; }

    private long getDeviation() { return deviation; }

    long getExecuteTime() { return executeTime; }

    private long getAdjustedDuration() { return adjustedDuration; }

    private void setAdjustedDuration(long adjustedDuration) { this.adjustedDuration = adjustedDuration; }

    private String formatTime(long r){

        //long days = TimeUnit.MILLISECONDS.toDays(r);
        long hours = TimeUnit.MILLISECONDS.toHours(r);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(r) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(r));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(r) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(r));
        String res = "";

        //Pretty Print the time so it will always be in this format 00:00:00
        if( hours < 10 ) res = res + "0" + hours + ":";
        else res = res + hours + ":";

        if(minutes < 10) res = res + "0" + minutes + ":";
        else res = res + minutes + ":";

        if(seconds < 10) res = res + "0" + seconds;
        else res = res + seconds;

        return res;
    }

}
