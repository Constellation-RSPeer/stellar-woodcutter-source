package StellarWoodcutter.framework.breaks;

import org.rspeer.runetek.api.commons.StopWatch;

import java.util.Arrays;
import java.util.List;

public class BreakHandler {

    private List<Break> sleepyTime;
    private Break currentBreak;
    private StopWatch stopWatch;

    public BreakHandler(StopWatch stopWatch, Break... breaks){
        this.stopWatch = stopWatch;
        this.sleepyTime = Arrays.asList(breaks);
        sortByDuration();
    }

    //TODO: Handle skipping over breaks after a long break
    public boolean shouldBreak() {
        for(Break b : sleepyTime){
            if(stopWatch.getElapsed().toMillis() >= b.getExecuteTime()){
                currentBreak = b;
                return true;
            }
        }
        return false;
    }

    private void sortByDuration(){
        sleepyTime.sort((o1, o2) -> (int) (o1.getDuration() - o2.getDuration()));
    }

    public void startBreak(){
        currentBreak.execute();
    }

}
