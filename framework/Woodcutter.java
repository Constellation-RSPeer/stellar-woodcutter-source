package StellarWoodcutter.framework;

import StellarWoodcutter.res.DataStore;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Distance;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.DepositBox;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.ui.Log;

import java.util.function.Predicate;

public final class Woodcutter {

    public static void logout(){
        if(Game.isLoggedIn()){
            if(Game.logout()){
                Time.sleepUntil(() -> !Game.isLoggedIn(), 50, 25000);
            }
        }
    }

    public static boolean isValid(SceneObject obj){
        if(obj != null){
            Position p = obj.getPosition();
            if (p == null) {
                return false;
            }
            SceneObject objAtPos = SceneObjects.getFirstAt(p);
            if (objAtPos == null || objAtPos.getDefinition() == null) {
                return false;
            }
            short[] colors = objAtPos.getDefinition().getNewColors();
            if (colors == null || colors.length == 0) {
                return false;
            }

            return true;
        }
        return false;
    }

    public static boolean isCutting(){
        if(DataStore.getCurrentTree() != null) {
            if(!Players.getLocal().isAnimating()){
                return false;
            }
            Position p = DataStore.getCurrentTree().getPosition();
            if (p == null) {
                return false;
            }
            SceneObject tree = SceneObjects.getFirstAt(p);
            if (tree == null || tree.getDefinition() == null) {
                return false;
            }

            if(tree.getDefinition() == null){
                return false;
            }

            return true;
        }
        return false;
    }

    public static SceneObject findNextTree(){
        try{
            final Predicate<SceneObject> NEXT = t ->
                    !t.equals(DataStore.getCurrentTree())
                            && t.getName().equals(DataStore.getTreeType().getName())
                            && DataStore.getTreeArea().contains(t.getPosition());
            return SceneObjects.getBest( (o1, o2) -> {
                double d1 = Distance.between(Players.getLocal(), o1);
                double d2 = Distance.between(Players.getLocal(), o2);
                int res = (int) (Math.round(d1) - Math.round(d2));
                return res;
            }, NEXT);
        }catch (Exception e){
            return null;
        }
    }

    /** BANKING **/
    public static boolean openBank(){
        if(Bank.isOpen() || DepositBox.isOpen()){
            return true;
        }
        else if(!Bank.isOpen() || !DepositBox.isOpen()){
            if(DataStore.getClosestBank() != null){
                String closest = DataStore.getClosestBank().getName();
                SceneObject bank = SceneObjects.getNearest(closest);
                if(bank != null) {
                    String[] actions = bank.getActions();
                    String ACTION = "Bank";
                    for (int i = 0; i < actions.length; i++) {
                        if (actions[i].equals("Deposit")) ACTION = "Deposit";
                        else if (actions[i].equals("Use")) ACTION = "Use";
                    }
                    if (bank.interact(ACTION)) {
                        Time.sleepUntil(() -> (Bank.isOpen() || DepositBox.isOpen()), 50, 4000);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean depositAll(Predicate<? super Item> predicate){
        if(Inventory.contains(predicate)){
            if(Bank.isOpen()) return Bank.depositAll(predicate);
        }
        return Inventory.contains(predicate) && DepositBox.depositAll(predicate);
    }

    public static boolean depositAll(String item) {
        if (Inventory.contains(item)) {
            if (Bank.isOpen()) return Bank.depositAll(item);
        }
        return Inventory.contains(item) && DepositBox.depositAll(item);
    }
    public static boolean withdraw(Predicate<? super Item> predicate, int amount) { return Bank.contains(predicate) && Bank.withdraw(predicate, amount); }

    public static boolean withdrawAll(Predicate<? super Item> predicate ){ return Bank.contains(predicate) && Bank.withdrawAll(predicate); }

}
