package StellarWoodcutter.framework;

public enum Tree {

    TREE("Tree", 1),
    OAK("Oak", 15),
    WILLOW("Willow", 30),
    TEAK("Teak", 35),
    MAPLE("Maple tree", 45),
    MAHOGANY("Mahogany", 50),
    YEW("Yew", 60),
    MAGIC("Magic tree", 75);

    private String name;
    private int level;

    Tree(String name, int level){
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }
}
