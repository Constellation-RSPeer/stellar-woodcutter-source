package StellarWoodcutter.framework;

public enum DropSettings {

    DROP_FULL,
    DROP_C1D1,
    DROP_C2D2,
    BANK;

    @Override
    public String toString() {
        return this.name().toLowerCase().trim().replace("_", " ");
    }
}
