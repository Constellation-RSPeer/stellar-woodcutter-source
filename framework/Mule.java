package StellarWoodcutter.framework;

import StellarWoodcutter.res.DataStore;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Distance;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Trade;
import org.rspeer.runetek.api.component.WorldHopper;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;

import java.awt.event.KeyEvent;
import java.util.function.Predicate;

public final class Mule {

    private static boolean triggered;
    private static int muleWorld;
    private static String muleName;
    private static int mulePhase;
    private static MuleLocation muleLocation;
    private static int currentWorld;

    private static final int WALK_TO_BANK = 0;
    private static final int GET_MULE_ITEMS = 1;
    private static final int HOP_WORLD = 2;
    private static final int WALK_TO_MULE = 3;
    private static final int TRADE_MULE = 4;
    private static final int HOP_BACK = 5;

    public static boolean isTriggered() { return triggered; }

    public static void toggleTrigger(boolean on){
        if(on){ triggered = true; }
        else{ triggered = false; }
    }

    public static void validateTrigger(KeyEvent keyEvent){
        if(keyEvent.getKeyChar() == 'm'){ triggered = true; }
        if(keyEvent.getKeyChar() == 'c'){ triggered = false; }
    }

    public static void evaluatePhase(){
        switch (mulePhase){
            case WALK_TO_BANK:
                currentWorld = Worlds.getCurrent();
                Walker.walkToClosestBank();
                if(Distance.between(DataStore.getClosestBank().getPosition(), Players.getLocal()) <= 15){
                    mulePhase++;
                }
                else{
                    Time.sleepWhile(() -> Movement.isDestinationSet(), 4000);
                }
                break;
            case GET_MULE_ITEMS:
                withdrawItems();
                break;
            case HOP_WORLD:
                if(Worlds.getCurrent() != muleWorld){
                    if(WorldHopper.hopTo(muleWorld)){
                        Time.sleepUntil(() -> Worlds.getCurrent() == muleWorld, 6000);
                    }
                }
                if(Worlds.getCurrent() == muleWorld){
                    mulePhase++;
                }
                break;
            case WALK_TO_MULE:
                Position p = muleLocation.getPosition();
                Walker.walkToPosition(p);
                Player mule = Players.getNearest(muleName);
                if(mule != null) {
                    mulePhase++;
                }
                else{ Time.sleepWhile(() -> Movement.isDestinationSet(), 4000); }
                break;
            case TRADE_MULE:
                if(!Trade.isOpen()){
                    mule = Players.getNearest(muleName);
                    if(mule != null && mule.interact("Trade with")){
                        Time.sleepUntil(() -> Trade.isOpen(), 5000);
                    }
                }
                if(Trade.isOpen()){
                    final Predicate<Item> NOTED_LOGS = logs -> logs.getName().toLowerCase().contains("logs") && logs.isNoted();
                    while(Inventory.contains(NOTED_LOGS)){
                        Trade.offerAll(NOTED_LOGS);
                        Time.sleep(400, 650);
                    }
                    Time.sleep(450, 700);
                    if(Trade.contains(true, NOTED_LOGS) && Trade.accept()){
                        Time.sleepUntil(() -> Trade.isOpen(true), 6000);
                    }
                    if(Trade.isOpen(true) && Trade.accept()){
                        Time.sleepUntil(() -> !Trade.isOpen(), 6000);
                    }
                    if(!Trade.isOpen()){
                        mulePhase++;
                    }
                }
                break;
            case HOP_BACK:
                if(Worlds.getCurrent() != currentWorld){
                    if(WorldHopper.hopTo(currentWorld)){
                        Time.sleepUntil(() -> Worlds.getCurrent() == currentWorld, 6000);
                    }
                }
                if(Worlds.getCurrent() == currentWorld){
                    mulePhase = 0;
                    triggered = false;
                }
                break;
        }
    }

    private static void withdrawItems(){
        if(!Bank.isOpen() && Bank.open()){
            Time.sleepUntil(() -> Bank.isOpen(), 5500);
        }
        if(Bank.isOpen()){
            final Predicate<Item> unnotedLogs = p -> !p.isNoted() && p.getName().toLowerCase().contains("logs");
            final Predicate<Item> logs = p -> p.getName().toLowerCase().contains("logs") && !p.getName().toLowerCase().contains("split");
            if(Inventory.contains(unnotedLogs) && Bank.depositAll(unnotedLogs)){
                Time.sleepUntil(() -> !Inventory.contains(unnotedLogs), 2000);
            }
            if(Bank.getWithdrawMode() == Bank.WithdrawMode.ITEM && Bank.setWithdrawMode(Bank.WithdrawMode.NOTE)){
                Time.sleepUntil(() -> Bank.getWithdrawMode() == Bank.WithdrawMode.NOTE, 2000);
            }
            if(Bank.withdrawAll(logs)){
                Time.sleepUntil(() -> !Bank.contains(logs), 7000);
            }
            if(Inventory.contains(logs) && !Bank.contains(logs)){
                Bank.close();
                mulePhase++;
            }
        }
    }

    public static MuleLocation getMuleLocation() { return muleLocation; }
    public static void setMuleLocation(MuleLocation muleLocation) { Mule.muleLocation = muleLocation; }
    public static void setMuleWorld(int muleWorld) { Mule.muleWorld = muleWorld; }
    public static void setMuleName(String muleName) { Mule.muleName = muleName; }
}
