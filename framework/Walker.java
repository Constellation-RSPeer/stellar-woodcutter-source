package StellarWoodcutter.framework;

import StellarWoodcutter.res.DataStore;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Distance;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.path.Path;
import org.rspeer.runetek.api.movement.pathfinding.executor.PathExecutor;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.ui.Log;

import java.util.List;

public final class Walker {

    private static final PathExecutor pathExecutor = new PathExecutor();

    public static void walkToClosestBank(){
        if (DataStore.getClosestBank() != null) {
            Position walk = DataStore.getClosestBank().getPosition();
            Path path = Movement.buildPath(walk);
            if (path != null) {
                Area bankArea = Area.surrounding(walk, 2);
                DataStore.setBankArea(bankArea);
                if (Movement.getDestination() != null && isFinalTile(walk)) {
                    Time.sleep(100);
                }
                else {
                    path.walk(pathExecutor);
                }
            } else {
                Log.info("No path to bank available");
            }
        }
    }

    public static void walkToPosition(Position position){
        Path path = Movement.buildPath(position);
        if (Movement.getDestination() != null
                && isFinalTile(position)){
            Time.sleep(100);
        } else {
            if(path != null) path.walk(pathExecutor);
        }
    }

    private static boolean isFinalTile(Position p){
        Position dest = Movement.getDestination();
        if(p.equals(dest)){
            return true;
        }
        return false;
    }

    private static boolean checkDist(){
        if(Movement.getDestination() != null){
            Position dest = Movement.getDestination();
            int max = Random.nextInt(9,13);
            double dist = Distance.between(Players.getLocal(), dest);
            return dist < max;
        }
        return true;
    }
}
