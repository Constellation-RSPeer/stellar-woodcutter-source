package StellarWoodcutter.framework;

import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.movement.position.Position;

public enum MuleLocation {

    SEERS_VILLAGE("Seers Village", BankLocation.SEERS_VILLAGE.getPosition()),
    GRAND_EXCHANGE("Grand Exchange", BankLocation.GRAND_EXCHANGE.getPosition())
    ;

    MuleLocation(String name, Position position){
        this.name = name;
        this.position = position;
    }

    private String name;
    private Position position;

    public Position getPosition() { return position; }
}
