package StellarWoodcutter.framework;


import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import StellarWoodcutter.settings.PreferencesLoader;
import StellarWoodcutter.settings.UserPref;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.ClientSupplier;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.ui.Log;

import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StellarGui {

    private JFrame frame;
    private JTextField txtFerrismuler;
    private JTextField textField;
    private int w;
    private int h;
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    StellarGui window = new StellarGui();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public StellarGui() {
        PreferencesLoader.createFolderIfNotExists();
        w = 1;
        h = 1;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 420, 695);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        JPanel titlePanel = new JPanel();
        titlePanel.setBackground(new Color(105, 105, 105));
        titlePanel.setForeground(Color.GRAY);
        frame.getContentPane().add(titlePanel, BorderLayout.NORTH);

        JLabel lblStellarWoodcutter = new JLabel("Stellar Woodcutter");
        lblStellarWoodcutter.setBackground(Color.GRAY);
        lblStellarWoodcutter.setForeground(Color.WHITE);
        lblStellarWoodcutter.setFont(new Font("Tahoma", Font.PLAIN, 30));
        titlePanel.add(lblStellarWoodcutter);

        JPanel mainPanel = new JPanel();
        frame.getContentPane().add(mainPanel, BorderLayout.CENTER);
        mainPanel.setLayout(null);

        JPanel treeOptionsPanel = new JPanel();
        treeOptionsPanel.setBounds(15, 16, 368, 100);
        mainPanel.add(treeOptionsPanel);

        treeOptionsPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5,5,5,5),
                BorderFactory.createTitledBorder("Tree Options")
        ));
        treeOptionsPanel.setLayout(null);

        int l = Tree.values().length;
        String[] trees = new String[l];
        for(int i = 0; i < l; i++){
            if(Tree.values()[i].getName().equals("Maple tree")){
                trees[i] = "Maple";
            }
            else if(Tree.values()[i].getName().equals("Magic tree")){
                trees[i] = "Magic";
            }
            else {
                trees[i] = Tree.values()[i].getName();
            }
        }

        JComboBox comboBox = new JComboBox(trees);
        comboBox.setBounds(27, 41, 168, 26);
        treeOptionsPanel.add(comboBox);

        JCheckBox chckbxBank = new JCheckBox("Use Bank");
        chckbxBank.setBounds(218, 40, 139, 29);
        treeOptionsPanel.add(chckbxBank);

        JPanel treeAreaPanel = new JPanel();
        treeAreaPanel.setBounds(15, 119, 368, 100);
        mainPanel.add(treeAreaPanel);

        treeAreaPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5,5,5,5),
                BorderFactory.createTitledBorder("Tree Area")
        ));
        treeAreaPanel.setLayout(null);

        JLabel lblWidth = new JLabel("Width");
        lblWidth.setBounds(27, 49, 69, 20);
        treeAreaPanel.add(lblWidth);

        JSpinner spinner = new JSpinner();
        spinner.setBounds(82, 46, 59, 26);
        spinner.setValue(1);
        treeAreaPanel.add(spinner);
        JComponent comp = spinner.getEditor();
        JFormattedTextField jtf = (JFormattedTextField) comp.getComponent(0);
        DefaultFormatter formatter = (DefaultFormatter) jtf.getFormatter();
        formatter.setCommitsOnValidEdit(true);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                try {
                    w = (int) spinner.getValue();
                    Player local = Players.getLocal();
                    Area work = Area.rectangular(local.getX()-w, local.getY()-h,
                            local.getX()+w, local.getY() + h);
                    DataStore.setTreeArea(work);
                }
                catch(Exception ex){
                    Log.info("Error getting tree area");
                }
            }
        });


        JLabel lblHeight = new JLabel("Height");
        lblHeight.setBounds(193, 49, 69, 20);
        treeAreaPanel.add(lblHeight);

        JSpinner spinner_1 = new JSpinner();
        spinner_1.setBounds(247, 46, 59, 26);
        spinner_1.setValue(1);
        treeAreaPanel.add(spinner_1);
        JComponent comp1 = spinner_1.getEditor();
        JFormattedTextField jtf1 = (JFormattedTextField) comp1.getComponent(0);
        DefaultFormatter formatter1 = (DefaultFormatter) jtf1.getFormatter();
        formatter1.setCommitsOnValidEdit(true);
        spinner_1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                try {
                    h = (int) spinner_1.getValue();
                    Player local = Players.getLocal();
                    Area work = Area.rectangular(local.getX()-w, local.getY()-h,
                            local.getX()+w, local.getY() + h);
                    DataStore.setTreeArea(work);
                }
                catch(Exception ex){
                    Log.info("Error getting tree area");
                }
            }
        });

        int len = Speed.values().length;
        String[] speeds = new String[len];
        for(int i = 0; i < len; i++){
            String sub = Speed.values()[i].toString().substring(1);
            char cap = Speed.values()[i].toString().toUpperCase().charAt(0);
            speeds[i] = cap + sub;
        }

        JPanel otherOptionsPanel = new JPanel();
        otherOptionsPanel.setBounds(15, 221, 368, 138);
        mainPanel.add(otherOptionsPanel);

        otherOptionsPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5,5,5,5),
                BorderFactory.createTitledBorder("Other Options")
        ));
        otherOptionsPanel.setLayout(null);

        JLabel lblSpeed = new JLabel("Speed");
        lblSpeed.setBounds(27, 42, 82, 20);
        otherOptionsPanel.add(lblSpeed);

        JComboBox comboBox_1 = new JComboBox(speeds);
        comboBox_1.setBounds(150, 39, 159, 26);
        comboBox_1.setSelectedIndex(3);
        otherOptionsPanel.add(comboBox_1);

        JCheckBox chckbxUseAxeSpecial = new JCheckBox("Use Axe Special");
        chckbxUseAxeSpecial.setBounds(150, 82, 207, 29);
        otherOptionsPanel.add(chckbxUseAxeSpecial);

        JLabel lblDragonAxe = new JLabel("Dragon Axe");
        lblDragonAxe.setBounds(27, 86, 112, 20);
        otherOptionsPanel.add(lblDragonAxe);

        String[] locations  = {"Grand Exchange", "Seers Village"};
        JPanel mulePanel = new JPanel();
        mulePanel.setLayout(null);
        mulePanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5,5,5,5),
                BorderFactory.createTitledBorder("Muling")
        ));
        mulePanel.setBounds(15, 361, 368, 190);
        mainPanel.add(mulePanel);

        JLabel labelName = new JLabel("Mule Name:");
        labelName.setBounds(27, 42, 138, 25);
        mulePanel.add(labelName);

        txtFerrismuler = new JTextField();
        txtFerrismuler.setText("FerrisMuler");
        txtFerrismuler.setBounds(180, 41, 160, 26);
        mulePanel.add(txtFerrismuler);
        txtFerrismuler.setColumns(10);

        JLabel lblMuleWorld = new JLabel("Mule World:");
        lblMuleWorld.setBounds(27, 84, 138, 25);
        mulePanel.add(lblMuleWorld);

        textField = new JTextField();
        textField.setText("308");
        textField.setColumns(10);
        textField.setBounds(180, 83, 160, 26);
        mulePanel.add(textField);

        JLabel lblMuleLocation = new JLabel("Mule Location:");
        lblMuleLocation.setBounds(27, 130, 138, 20);
        mulePanel.add(lblMuleLocation);

        JComboBox comboBox_2 = new JComboBox(locations);
        comboBox_2.setBounds(180, 130, 160, 26);
        mulePanel.add(comboBox_2);

        JButton btnStartScript = new JButton("Start Script");
        btnStartScript.setBounds(15, 556, 368, 29);
        mainPanel.add(btnStartScript);

        UserPref prevSettings = PreferencesLoader.loadJson(PreferencesLoader.STYPE);
        if(prevSettings != null){
            String location = prevSettings.getMuleLocation();
            switch (location){
                case "Grand Exchange":
                    comboBox_2.setSelectedIndex(0);
                    break;
                case "Seers Village":
                    comboBox_2.setSelectedIndex(1);
                    break;
            }
            int world = prevSettings.getMuleWorld();
            textField.setText("" + world);
            String name = prevSettings.getMuleName();
            txtFerrismuler.setText(name);
        }

        btnStartScript.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(comboBox.getSelectedItem() != null){
                    String tree = (String) comboBox.getSelectedItem();
                    tree = tree.toLowerCase();
                    //Log.info(tree);
                    if(tree.equals("tree")) DataStore.setTreeType(Tree.TREE);
                    if(tree.equals("oak")) DataStore.setTreeType(Tree.OAK);
                    if(tree.equals("willow")) DataStore.setTreeType(Tree.WILLOW);
                    if(tree.equals("teak")) DataStore.setTreeType(Tree.TEAK);
                    if(tree.equals("maple")) DataStore.setTreeType(Tree.MAPLE);
                    if(tree.equals("mahogany")) DataStore.setTreeType(Tree.MAHOGANY);
                    if(tree.equals("yew")) DataStore.setTreeType(Tree.YEW);
                    if(tree.equals("magic")) DataStore.setTreeType(Tree.MAGIC);
                    //Log.info(UserPref.getTreeType().getName());
                }
                if(chckbxBank.isSelected()){
                    Settings.setBanking(true);
                    DataStore.setDropType(DropSettings.BANK);
                }
                else{
                    if(DataStore.getTreeType().getName().equals("Tree")) {
                        if (Random.nextInt(0, 100) < 50) {
                            DataStore.setDropType(DropSettings.DROP_C1D1);
                        } else {
                            DataStore.setDropType(DropSettings.DROP_C2D2);
                        }
                    }
                    else{
                        DataStore.setDropType(DropSettings.DROP_FULL);
                    }
                }
                try {
                    int valueX = (int) spinner.getValue();
                    int valueY = (int) spinner_1.getValue();
                    if(valueX < 1 || valueY < 1){
                        int space = 1;
                        Player local = Players.getLocal();
                        Area work = Area.rectangular(local.getX()-space, local.getY()-space,
                                local.getX()+space, local.getY() + space);
                        DataStore.setTreeArea(work);
                    }
                }catch (Exception ex){
                    Log.info("Error found in Tree Area Width or Height!");
                }

                if(comboBox_1.getSelectedItem() != null){
                    String speed = (String) comboBox_1.getSelectedItem();
                    speed = speed.toLowerCase();
                    if(speed.equals("afk")) Settings.setSpeed(Speed.AFK);
                    if(speed.equals("slow")) Settings.setSpeed(Speed.SLOW);
                    if(speed.equals("medium")) Settings.setSpeed(Speed.MEDIUM);
                    if(speed.equals("normal")) Settings.setSpeed(Speed.NORMAL);
                    if(speed.equals("fast")) Settings.setSpeed(Speed.FAST);
                }
                if(chckbxUseAxeSpecial.isSelected()){
                    DataStore.setUsingSpecialAxe(true);
                }

                final int WORLD = Integer.parseInt(textField.getText());
                final String NAME = txtFerrismuler.getText();
                Mule.setMuleName(NAME);
                Mule.setMuleWorld(WORLD);
                final String LOCATION = (String) comboBox_2.getSelectedItem();
                switch (LOCATION){
                    case "Grand Exchange":
                        Mule.setMuleLocation(MuleLocation.GRAND_EXCHANGE);
                        break;
                    case "Seers Village":
                        Mule.setMuleLocation(MuleLocation.SEERS_VILLAGE);
                        break;
                }

                UserPref pref = new UserPref(WORLD, NAME, LOCATION);
                PreferencesLoader.saveJson(pref);
                frame.setVisible(false);
                DataStore.setStart(true);
            }
        });


        frame.setVisible(true);
        frame.setLocationRelativeTo(ClientSupplier.get().getCanvas());
    }
}


/*
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.UserPref;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.ClientSupplier;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.ui.Log;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StellarGui {

    private JFrame frame;
    private int w;
    private int h;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    StellarGui window = new StellarGui();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public StellarGui() {
        w = 1;
        h = 1;
        initialize();
    }

    private void initialize() {

        frame = new JFrame();
        frame.getContentPane().setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));

        JPanel mainPanel = new JPanel();
        frame.getContentPane().add(mainPanel);
        mainPanel.setLayout(null);

        JPanel titlePanel = new JPanel();
        titlePanel.setBounds(26, 0, 308, 52);
        mainPanel.add(titlePanel);
        titlePanel.setLayout(null);

        JLabel lblStellarAioWoodcutter = new JLabel("Stellar Woodcutter");
        lblStellarAioWoodcutter.setFont(new Font("Trebuchet MS", Font.PLAIN, 26));
        lblStellarAioWoodcutter.setBounds(39, 0, 237, 52);
        titlePanel.add(lblStellarAioWoodcutter);

        JPanel treeOptionsPanel = new JPanel();
        treeOptionsPanel.setBounds(15, 64, 335, 52);
        mainPanel.add(treeOptionsPanel);
        treeOptionsPanel.setLayout(null);

        JLabel lblTree = new JLabel("Tree");
        lblTree.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        lblTree.setBounds(15, 16, 69, 20);
        treeOptionsPanel.add(lblTree);

        int l = Tree.values().length;
        String[] trees = new String[l];
        for(int i = 0; i < l; i++){
            if(Tree.values()[i].getName().equals("Maple tree")){
                trees[i] = "Maple";
            }
            else if(Tree.values()[i].getName().equals("Magic tree")){
                trees[i] = "Magic";
            }
            else {
                trees[i] = Tree.values()[i].getName();
            }
        }=======================

        JComboBox comboBox = new JComboBox(trees);
        comboBox.setBounds(77, 13, 160, 26);
        treeOptionsPanel.add(comboBox);

        JCheckBox chckbxBank = new JCheckBox("Bank?");
        chckbxBank.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        chckbxBank.setBounds(248, 12, 104, 29);
        treeOptionsPanel.add(chckbxBank);

        JPanel startPanel = new JPanel();
        startPanel.setBounds(15, 293, 335, 55);
        mainPanel.add(startPanel);
        startPanel.setLayout(new GridLayout(1, 0, 0, 0));

        JButton btnStartScript = new JButton("Start Script");
        btnStartScript.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        startPanel.add(btnStartScript);

        JPanel panel = new JPanel();
        panel.setBounds(15, 122, 335, 70);
        mainPanel.add(panel);
        panel.setLayout(null);

        JLabel lblTreeArea = new JLabel("Tree Area");
        lblTreeArea.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        lblTreeArea.setBounds(15, 0, 95, 20);
        panel.add(lblTreeArea);

        JLabel lblWidth = new JLabel("Width");
        lblWidth.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        lblWidth.setBounds(25, 36, 69, 20);
        panel.add(lblWidth);

        JSpinner spinner = new JSpinner();
        spinner.setBounds(80, 33, 57, 26);
        spinner.setValue(1);
        panel.add(spinner);
        JComponent comp = spinner.getEditor();
        JFormattedTextField jtf = (JFormattedTextField) comp.getComponent(0);
        DefaultFormatter formatter = (DefaultFormatter) jtf.getFormatter();
        formatter.setCommitsOnValidEdit(true);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                try {
                    w = (int) spinner.getValue();
                    Player local = Players.getLocal();
                    Area work = Area.rectangular(local.getX()-w, local.getY()-h,
                            local.getX()+w, local.getY() + h);
                    DataStore.setTreeArea(work);
                }
                catch(Exception ex){
                    Log.info("Error getting tree area");
                }
            }
        });

        JLabel lblHeight = new JLabel("Height");
        lblHeight.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        lblHeight.setBounds(187, 36, 57, 20);
        panel.add(lblHeight);

        JSpinner spinner_1 = new JSpinner();
        spinner_1.setBounds(247, 33, 57, 26);
        spinner_1.setValue(1);
        panel.add(spinner_1);
        JComponent comp1 = spinner_1.getEditor();
        JFormattedTextField jtf1 = (JFormattedTextField) comp1.getComponent(0);
        DefaultFormatter formatter1 = (DefaultFormatter) jtf1.getFormatter();
        formatter1.setCommitsOnValidEdit(true);
        spinner_1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                try {
                    h = (int) spinner_1.getValue();
                    Player local = Players.getLocal();
                    Area work = Area.rectangular(local.getX()-w, local.getY()-h,
                            local.getX()+w, local.getY() + h);
                    DataStore.setTreeArea(work);
                }
                catch(Exception ex){
                    Log.info("Error getting tree area");
                }
            }
        });

        JPanel panelOptions = new JPanel();
        panelOptions.setBounds(15, 192, 335, 85);
        mainPanel.add(panelOptions);
        panelOptions.setLayout(null);

        JLabel lblOther = new JLabel("Speed");
        lblOther.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        lblOther.setBounds(15, 16, 60, 20);
        panelOptions.add(lblOther);

        int len = Speed.values().length;
        String[] speeds = new String[len];
        for(int i = 0; i < len; i++){
            String sub = Speed.values()[i].toString().substring(1);
            char cap = Speed.values()[i].toString().toUpperCase().charAt(0);
            speeds[i] = cap + sub;
        }

        JComboBox comboBox_1 = new JComboBox(speeds);
        comboBox_1.setBounds(77, 13, 160, 26);
        //Set default speed to normal
        comboBox_1.setSelectedIndex(3);
        panelOptions.add(comboBox_1);

        JCheckBox chckbxAxeSpecial = new JCheckBox("Axe Special?");
        chckbxAxeSpecial.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        chckbxAxeSpecial.setBounds(15, 51, 139, 29);
        panelOptions.add(chckbxAxeSpecial);

        JCheckBox chckbxDismissRandoms = new JCheckBox("Dismiss Randoms");
        chckbxDismissRandoms.setFont(new Font("Trebuchet MS", Font.PLAIN, 17));
        chckbxDismissRandoms.setBounds(151, 51, 184, 29);
        panelOptions.add(chckbxDismissRandoms);

        frame.setBounds(100, 100, 385, 420);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.setVisible(true);
        frame.setLocationRelativeTo(ClientSupplier.get().getCanvas());

        btnStartScript.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(comboBox.getSelectedItem() != null){
                    String tree = (String) comboBox.getSelectedItem();
                    tree = tree.toLowerCase();
                    //Log.info(tree);
                    if(tree.equals("tree")) DataStore.setTreeType(Tree.TREE);
                    if(tree.equals("oak")) DataStore.setTreeType(Tree.OAK);
                    if(tree.equals("willow")) DataStore.setTreeType(Tree.WILLOW);
                    if(tree.equals("teak")) DataStore.setTreeType(Tree.TEAK);
                    if(tree.equals("maple")) DataStore.setTreeType(Tree.MAPLE);
                    if(tree.equals("mahogany")) DataStore.setTreeType(Tree.MAHOGANY);
                    if(tree.equals("yew")) DataStore.setTreeType(Tree.YEW);
                    if(tree.equals("magic")) DataStore.setTreeType(Tree.MAGIC);
                    //Log.info(UserPref.getTreeType().getName());
                }
                if(chckbxBank.isSelected()){
                    UserPref.setBanking(true);
                    DataStore.setDropType(DropSettings.BANK);
                }
                else{
                    if(DataStore.getTreeType().getName().equals("Tree")) {
                        if (Random.nextInt(0, 100) < 50) {
                            DataStore.setDropType(DropSettings.DROP_C1D1);
                        } else {
                            DataStore.setDropType(DropSettings.DROP_C2D2);
                        }
                    }
                    else{
                        DataStore.setDropType(DropSettings.DROP_FULL);
                    }
                }
                try {
                    int valueX = (int) spinner.getValue();
                    int valueY = (int) spinner_1.getValue();
                    if(valueX < 1 || valueY < 1){
                        int space = 1;
                        Player local = Players.getLocal();
                        Area work = Area.rectangular(local.getX()-space, local.getY()-space,
                                local.getX()+space, local.getY() + space);
                        DataStore.setTreeArea(work);
                    }
                }catch (Exception ex){
                    Log.info("Error found in Tree Area Width or Height!");
                }

                if(comboBox_1.getSelectedItem() != null){
                    String speed = (String) comboBox_1.getSelectedItem();
                    speed = speed.toLowerCase();
                    if(speed.equals("afk")) UserPref.setSpeed(Speed.AFK);
                    if(speed.equals("slow")) UserPref.setSpeed(Speed.SLOW);
                    if(speed.equals("medium")) UserPref.setSpeed(Speed.MEDIUM);
                    if(speed.equals("normal")) UserPref.setSpeed(Speed.NORMAL);
                    if(speed.equals("fast")) UserPref.setSpeed(Speed.FAST);
                }
                if(chckbxAxeSpecial.isSelected()){
                    DataStore.setUsingSpecialAxe(true);
                }
                frame.setVisible(false);
                DataStore.setStart(true);
            }
        });

    }

}
*/