package StellarWoodcutter;

import StellarWoodcutter.framework.Mule;
import StellarWoodcutter.framework.Speed;
import StellarWoodcutter.framework.StellarGui;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import StellarWoodcutter.tasks.*;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Distance;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.*;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.Projection;
import org.rspeer.runetek.event.listeners.KeyInputListener;
import org.rspeer.runetek.event.listeners.MouseInputListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.concurrent.TimeUnit;

@ScriptMeta(name = "Stellar Woodcutter", developer = "Woodcutter",
        desc = "Cut anywhere, bank anywhere. Define your own woodcutting area. Only supports bank booths" +
                " accessible by the RSPeer Web.")
public class StellarWoodcutter extends Script implements RenderListener, MouseInputListener {

    //private final Task[] TASKS = {new Cut(), new Drop(), new ContinueDialog(),
    //        new NextTree(), new Traverse(), new UseBank()};
    private final Task[] TASKS = {new Cut(), new Drop(), new PickNest(), new ContinueDialog(),
            new Traverse(), new UseBank(), new UseAxeSpecial()};
    private final Rectangle VIEWPORT = new Rectangle (0,0, 520,340);
    private final Skill WC = Skill.WOODCUTTING;
    private final Area WOODCUTTING_GUILD = Area.rectangular(1557, 3520, 1660, 3470);

    //Paint variables
    private Font helvitica = new Font("Helvetica", Font.PLAIN, 13);
    private Font centuryGothic = new Font("CenturyGothic", Font.BOLD, 14);
    private Image bg = getImage("https://i.imgur.com/N5EI2GD.png");
    private final String version = "0.18";
    private StopWatch sw;
    private long startExp;
    private long currExp;
    private int startLevel;
    private long cut = 0;
    private Point mousePoint;
    private final Point OFF_SCREEN = new Point(0,0);

    @Override
    public void onStart() {
        super.onStart();

        new StellarGui();
        Settings.setSpeed(Speed.NORMAL);

        int space = 1;
        Player local = Players.getLocal();
        Area work = Area.rectangular(local.getX()-space, local.getY()-space,
                local.getX()+space, local.getY() + space);
        DataStore.setTreeArea(work);

        DataStore.setRandomRun(Random.nextInt(50, 71));

        BankLocation closestDb = BankLocation.getNearestDepositBox();
        BankLocation closestChest = BankLocation.getNearestChest();
        BankLocation closestBooth = BankLocation.getNearestBooth();

        double distanceDb = Distance.between(local, closestDb.getPosition());
        double distanceChest = Distance.between(local, closestChest.getPosition());
        double distances[] = {distanceDb, distanceChest};
        double min = Distance.between(local, closestBooth.getPosition());
        DataStore.setClosestBank(BankLocation.getNearestBooth());
        for(int i = 0; i < 2; i++){
            if(min > distances[i]){
                if(i == 0) DataStore.setClosestBank(BankLocation.getNearestDepositBox());
                else DataStore.setClosestBank(BankLocation.getNearestChest());
            }
        }

        if(WOODCUTTING_GUILD.contains(Players.getLocal())){
            DataStore.setWoodcuttingGuild(true);
            DataStore.setClosestBank(BankLocation.WOODCUTTING_GUILD);
        }

        DataStore.setState("Waiting...");

        currExp = Skills.getExperience(WC);
        startLevel = Skills.getCurrentLevel(WC);
        startExp = currExp;
        sw = StopWatch.start();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    //TODO: Switch over to TaskScript and just override the loop
    @Override
    public int loop() {
        for(Task t : TASKS){
            checkRun();
            if(Mule.isTriggered()){
                Mule.evaluatePhase();

            }
            else {
                if (t.trigger()) {
                    int res = t.execute();
                    if (res != -1) {
                        Time.sleep(res);
                    } else {
                        setStopping(true);
                    }
                }
            }
        }
        return Settings.getLoopSpeed();
    }

    private void checkRun(){
        if(Game.isLoggedIn()) {
            if (Movement.getRunEnergy() > DataStore.getRandomRun()) {
                if (!Movement.isRunEnabled()) {
                    Movement.toggleRun(true);
                    DataStore.setRandomRun(Random.nextInt(50, 71));
                }
            }
        }
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        Graphics g = renderEvent.getSource();
        g.setColor(Color.GREEN);
        //Gussy up the text
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

        if(Mule.isTriggered()){
            g.setColor(Color.RED);
            g.drawString("Cancel Muling", 440, 330);
        }
        else{
            g.setColor(Color.GREEN);
            g.drawString("Start Muling", 446, 330);
        }
        Rectangle r = new Rectangle(437,315,82,20);
        g.drawRect(r.x, r.y, r.width, r.height);

        if(mousePoint != null && r.contains(mousePoint)){
            if(Mule.isTriggered()){ Mule.toggleTrigger(false); }
            else{ Mule.toggleTrigger(true); }
            mousePoint.setLocation(OFF_SCREEN);
        }

        if(!DataStore.isStart() && DataStore.getTreeArea() != null){
            g.setColor(Color.red);
            for(Position p : DataStore.getTreeArea().getTiles()){
                try {
                    Point mp = Projection.toMinimap(p);
                    if (mp != null) {
                        g2d.drawRect(mp.x, mp.y, 4, 4);
                        FinePosition fp = p.toFine();
                        ScreenPosition sp = Projection.fineToScreen(fp.getX(), fp.getY(), fp.getFloorLevel());
                        if (VIEWPORT.contains(sp.getX(), sp.getY())) {
                            p.outline(g);
                        }
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();

                }
            }
        }

        if(DataStore.getCurrentTree() != null){
            Position curr = DataStore.getCurrentTree().getPosition();
            curr.outline(g);
        }

        if(DataStore.getBankArea() != null){
            g.setColor(Color.BLUE);
            for(Position p : DataStore.getBankArea().getTiles()){
                p.outline(g);
            }
        }

        if(currExp != Skills.getExperience(WC)){
            currExp = Skills.getExperience(WC);
            cut++;
        }
        long xpGained = currExp-startExp;
        int levelsGained = Skills.getCurrentLevel(WC) - startLevel;

        Color transparentBlack = new Color(0,0,0, 100);
        Color translucentWhite = new Color(255, 255, 255, 150);

        //Draw the Background
        g2d.setColor(transparentBlack);
        g2d.fillRoundRect(5, 275, 390, 60, 15, 15);
        //g2d.fillRect(315, 215, 200, 120);
        g2d.setColor(translucentWhite);
        g2d.drawRoundRect(5, 275, 391, 61, 15, 15);

        //Draw Title
        g2d.setColor(transparentBlack);
        g2d.fillRoundRect(10, 240, 155, 23, 15, 15);
        g2d.setColor(translucentWhite);
        g2d.drawRoundRect(10, 240, 156, 24, 15, 15);
        //Draw avatar image
        g.drawImage(bg, 154, 236, null);
        //Draw Paint Text
        g2d.setColor(Color.WHITE);
        g2d.setFont(centuryGothic);
        g2d.drawString("Stellar Woodcutter", 20, 257);

        //Draw exp bar
        int barX = 165;
        int barY = 310;
        int barWidth = 210;
        int barHeight = 18;

        int currentExp = (int) currExp;
        int currentLevel = Skills.getCurrentLevel(WC);
        int currentLevelXp = Skills.getExperienceAt(currentLevel);
        int nextLevelXp = Skills.getExperienceAt(currentLevel+1);

        double x = nextLevelXp - currentExp;
        double y = nextLevelXp - currentLevelXp;
        double percentNL = (x/y) * 100;
        percentNL = 100.00 - percentNL;
        double dWidth = barWidth * (percentNL / 100);
        int iWidth = (int) dWidth;

        Color limeGreen = new Color(237, 33, 58, 255);
        Color lemonYellow = new Color(147, 41, 30, 255);
        //Color tealGreen = new Color(11, 163, 96, 255);
        //Color lightBlue = new Color(60, 186, 146, 255);
        Color lightGray1 = new Color(21, 21, 21, 255);
        Color lightGray2 = new Color(33, 33, 33, 255);
        Color purple = new Color(118, 75, 162, 200);

        GradientPaint color_bar = new GradientPaint(barX, barY, lemonYellow, (barX+iWidth), (barY+barHeight), limeGreen);
        GradientPaint colorBarBg = new GradientPaint(barX-1, barY-1, lightGray1, (barX+barWidth+1), (barY+barHeight+1), lightGray2);
        g2d.setColor(Color.gray);
        g2d.fillRect(barX-2, barY-2, barWidth+2,barHeight+2);
        g2d.setPaint(colorBarBg);
        g2d.fillRect(barX-1, barY-1, barWidth+1, barHeight+1);
        g2d.setPaint(color_bar);
        g2d.fillRect(barX, barY, iWidth, barHeight);

        long timeTL = 0;

        if(xpGained > 0){
            timeTL = (long) ((x / sw.getHourlyRate(xpGained)) * 3600000);
        }

        //Draw Paint info
        g2d.setFont(helvitica);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Runtime: " + sw.toElapsedString(), 20, 300);
        g2d.drawString("Logs cut: " + cut + " (" + (int) sw.getHourlyRate(cut) + ")", 20, 323);
        g2d.drawString("Exp Gained: " + xpGained + " (" + (int) sw.getHourlyRate(xpGained) + ")", 165, 300);
        //g2d.setPaint(purple);
        g2d.drawString("Level " + currentLevel + " (" + levelsGained + ")     " + formatTime(timeTL) +  "        " + (int) percentNL + "%", 170, 323);
        //g2d.setColor(Color.white);
        g2d.drawString("v" + version, 355, 295);
    }


    //Simple method to get an image from a URL, in this case an imgur link
    private Image getImage(String url){
        try{
            return ImageIO.read(new URL(url));
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //Used for formatting Time until next level used in paint
    private String formatTime(long r){

        //long days = TimeUnit.MILLISECONDS.toDays(r);
        long hours = TimeUnit.MILLISECONDS.toHours(r);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(r) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(r));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(r) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(r));
        String res = "";

        //Pretty Print the time so it will always be in this format 00:00:00
        if( hours < 10 ) res = res + "0" + hours + ":";
        else res = res + hours + ":";

        if(minutes < 10) res = res + "0" + minutes + ":";
        else res = res + minutes + ":";

        if(seconds < 10) res = res + "0" + seconds;
        else res = res + seconds;

        return res;
    }

    @Override
    public void notify(MouseEvent mouseEvent) {
        if(mouseEvent.getID() == MouseEvent.MOUSE_RELEASED) {
            mousePoint = mouseEvent.getPoint();
        }
    }
}
