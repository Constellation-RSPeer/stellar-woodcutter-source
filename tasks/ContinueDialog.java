package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Speed;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;

public class ContinueDialog implements Task {

    @Override
    public boolean trigger() {
        return DataStore.isStart()
                && Dialog.isOpen();
    }

    @Override
    public int execute() {
        if(Dialog.canContinue()){
            if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
                Time.sleep(Random.nextInt(250, 500));
            }
            else { Time.sleep(Settings.getLoopSpeed()); }
            if(Dialog.processContinue()){
                Time.sleepUntil(() -> Dialog.canContinue() || !Dialog.isOpen(), 50, 1500);
            }
        }
        if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
            return Random.nextInt(250, 500);
        }
        return Settings.getLoopSpeed();
    }
}
