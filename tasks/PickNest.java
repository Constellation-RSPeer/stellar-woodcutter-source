package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Speed;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Pickables;

import java.util.function.Predicate;

public class PickNest implements Task {

    private final Predicate<Pickable> NEST = p -> p.getName().toLowerCase().contains("nest");
    private final Predicate<Item> B_NEST = p -> p.getName().toLowerCase().contains("nest");
    private final String TAKE = "Take";

    @Override
    public boolean trigger() {
        return DataStore.isStart()
                && Settings.isBanking()
                && Pickables.getNearest(NEST) != null;
    }

    @Override
    public int execute() {
        Pickable nest = Pickables.getNearest(NEST);
        if(nest != null){
            if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW) {
                Time.sleep(Random.nextInt(250, 500));
            }
            else { Time.sleep(Settings.getLoopSpeed()); }
            if(nest.interact(TAKE)){
                final int count = Inventory.getCount(B_NEST);
                Time.sleepUntil(() -> Inventory.getCount(B_NEST) != count, 50, 3000);
            }
        }
        if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
            return Random.nextInt(250, 500);
        }
        return Settings.getLoopSpeed();
    }
}
