package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Speed;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.function.Predicate;


public class Drop implements Task {

    private final String LOGS = "Logs";
    private final String DROP = "Drop";
    private final Predicate<Item> PREDICATE_LOGS = i -> i.getName().equals(LOGS)
            || i.getName().contains(LOGS.toLowerCase());

    @Override
    public boolean trigger() {
        return DataStore.isStart() && evaluateDropType();
    }

    @Override
    public int execute() {
        DataStore.setState("Dropping logs");
        DataStore.setCurrentTree(null);
        switch (DataStore.getDropType().toString()){
            case "drop full":
                DataStore.setNextTree(null);
                Item[] inventory = Inventory.getItems();
                for(Item i : inventory){
                    if(i.getName().equals(LOGS) || i.getName().contains(LOGS.toLowerCase())){
                        int count = Inventory.getCount();
                        if(i.interact(DROP)){
                            Time.sleepWhile(() -> Inventory.getCount() == count, 50, 750);
                            //Time.sleep(UserPref.getLoopSpeed());
                        }
                    }
                }
                break;
            case "drop c1d1":
                Item l1 = Inventory.getFirst(PREDICATE_LOGS);
                if(l1 != null){
                    int count = Inventory.getCount(PREDICATE_LOGS);
                    if(l1.interact(DROP)){
                        Time.sleepWhile(() -> Inventory.getCount() == count, 50, 750);
                        //Time.sleep(UserPref.getLoopSpeed());
                    }
                }
                break;
            case "drop c2d2":
                Item[] l2 = Inventory.getItems(PREDICATE_LOGS);
                if(l2.length != 0){
                    for(Item i : l2) {
                        int count = Inventory.getCount(PREDICATE_LOGS);
                        if (i.interact(DROP)) {
                            Time.sleepWhile(() -> Inventory.getCount() == count, 50, 750);
                            //Time.sleep(UserPref.getLoopSpeed());
                        }
                    }
                }
                break;
            default:
                if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
                    return Random.nextInt(350, 600);
                }
                return Settings.getLoopSpeed();

        }
        if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
            return Random.nextInt(350, 600);
        }
        return Settings.getLoopSpeed();
    }

    private boolean evaluateDropType(){
        switch (DataStore.getDropType().toString()){
            case "drop full": return Inventory.isFull();
            case "drop c1d1": return Inventory.getCount(PREDICATE_LOGS) >= 1;
            case "drop c2d2": return Inventory.getCount(PREDICATE_LOGS) >= 2;
            default: return false;
        }
    }
}
