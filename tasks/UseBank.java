package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Woodcutter;
import StellarWoodcutter.framework.Speed;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Distance;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.DepositBox;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;

import java.util.function.Predicate;

public class UseBank implements Task {

    private final String LOGS = "Logs";
    private final Predicate<Item> PREDICATE_LOGS = i -> i.getName().equals(LOGS)
            || i.getName().contains(LOGS.toLowerCase());
    private final String NEST = "Bird nest";
    private final String CHEST = "Bank chest";

    @Override
    public boolean trigger() {
        return DataStore.isStart()
                && Inventory.isFull()
                && DataStore.getClosestBank() != null
                && Distance.between(DataStore.getClosestBank().getPosition(), Players.getLocal()) < 15;
    }

    @Override
    public int execute() {
        DataStore.setState("Using bank");
        //TODO: Trip in woodcutting guild to see if removal of this if statement is safe
        if(DataStore.isWoodcuttingGuild()){
            SceneObject chest = SceneObjects.getNearest(CHEST);
            if(chest != null){
                final String USE = "Use";
                if(chest.interact(USE)){
                    Time.sleepUntil(() -> Bank.isOpen(), 50, 4000);
                }
            }
        }
        else {
            Woodcutter.openBank();
        }
        Time.sleep(400,600);
        if(Bank.isOpen() || DepositBox.isOpen()){
            if(Woodcutter.depositAll(PREDICATE_LOGS)){
                Time.sleepUntil(() -> !Inventory.contains(PREDICATE_LOGS), 50, 1500);
                Time.sleep(400, 600);
            }
            if(Woodcutter.depositAll(NEST)){
                Time.sleepUntil(() -> !Inventory.contains(NEST), 50, 1500);
                Time.sleep(400, 600);
            }
            Bank.close();
        }
        if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
            return Random.nextInt(250, 500);
        }
        return Settings.getLoopSpeed();
    }

}
