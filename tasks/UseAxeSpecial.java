package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Speed;
import StellarWoodcutter.framework.Woodcutter;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.InterfaceAddress;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Combat;
import org.rspeer.runetek.api.component.tab.EquipmentSlot;

public class UseAxeSpecial implements Task {

    private final String[] SPECIAL_AXES = { "Dragon axe", "Infernal axe", "3rd age axe" };

    @Override
    public boolean trigger() {
        return DataStore.isUsingSpecialAxe() && Woodcutter.isCutting();
    }

    @Override
    public int execute() {
        //Check if special axe is equipped
        boolean equipped = false;
        for(int i = 0; i < 3; i++){
            if(EquipmentSlot.MAINHAND.getItemName().equals(SPECIAL_AXES[i])) equipped = true;
        }
        if(!equipped) DataStore.setUsingSpecialAxe(false);

        final String ACTION = "Combat Options";
        final InterfaceComponent COMBAT_TAB = Interfaces.getFirst(itf -> itf.containsAction(ACTION));
        if(COMBAT_TAB != null && (COMBAT_TAB.getMaterialId() != -1 || COMBAT_TAB.interact(ACTION))){
            Time.sleepUntil(() -> COMBAT_TAB.getMaterialId() != -1, 2000);
            Time.sleep(50, 250);

            //Get a component from combat tab to retrieve parent index;
            final InterfaceComponent COMBAT_TAB_INTERFACE = Interfaces.getFirst(itf -> itf.containsAction("Auto retaliate"));
            final int COMBAT_TAB_INDEX = COMBAT_TAB_INTERFACE.getParentIndex();

            //Get special text for identifying when special is 100%
            final InterfaceAddress SPECIAL_TEXT = new InterfaceAddress(
                    () -> Interfaces.getFirst(COMBAT_TAB_INDEX,
                            comp -> comp.getText().contains("%")));
            //Get special bar for using special
            final String SPEC_ACTION = "Use <col=00ff00>Special Attack</col>";
            final InterfaceAddress SPECIAL_BAR = new InterfaceAddress(
                    () -> Interfaces.getFirst(COMBAT_TAB_INDEX, comp -> comp.containsAction(SPEC_ACTION)));
            final InterfaceComponent STEXT = SPECIAL_TEXT.resolve();
            final InterfaceComponent SBAR = SPECIAL_BAR.resolve();
            if(SBAR != null){
                if(Combat.getSpecialEnergy() == 100){
                    SBAR.interact(SPEC_ACTION);
                }
            }
        }
        if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
            return Random.nextInt(350, 600);
        }
        return Settings.getLoopSpeed();
    }
}
