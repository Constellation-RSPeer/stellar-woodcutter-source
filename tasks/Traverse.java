package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Speed;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Distance;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.path.Path;
import org.rspeer.runetek.api.movement.pathfinding.executor.PathExecutor;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.ui.Log;

import java.util.List;

public class Traverse implements Task {

    private final PathExecutor pathExecutor = new PathExecutor();

    @Override
    public boolean trigger() {
        return DataStore.isStart()
                && (bank() || trees())
                && checkDist();
    }

    @Override
    public int execute() {
        DataStore.setCurrentTree(null);
        DataStore.setNextTree(null);
        if (bank()) {
            DataStore.setState("Walking to bank");
            if (DataStore.getClosestBank() != null) {
                Position walk = DataStore.getClosestBank().getPosition();
                Path path = Movement.buildPath(walk);
                if (path != null) {
                    //pathExecutor.setRandomizeAll(true);
                    Area bankArea = Area.surrounding(walk, 2);
                    DataStore.setBankArea(bankArea);
                    if (Movement.getDestination() != null && isFinalTile(true)) {
                        Time.sleep(100);
                    }
                    else {
                        path.walk(pathExecutor);
                    }
                } else {
                    Log.info("No path to bank available - stopping script");
                    return -1;
                }
            } else {
                //Script started logged out
                BankLocation closestDb = BankLocation.getNearestDepositBox();
                BankLocation closestChest = BankLocation.getNearestChest();
                BankLocation closestBooth = BankLocation.getNearestBooth();
                Player local = Players.getLocal();

                double distanceDb = Distance.between(local, closestDb.getPosition());
                double distanceChest = Distance.between(local, closestChest.getPosition());
                double distances[] = {distanceDb, distanceChest};
                double min = Distance.between(local, closestBooth.getPosition());
                DataStore.setClosestBank(BankLocation.getNearestBooth());
                for(int i = 0; i < 2; i++){
                    if(min > distances[i]){
                        if(i == 0) DataStore.setClosestBank(BankLocation.getNearestDepositBox());
                        else DataStore.setClosestBank(BankLocation.getNearestChest());
                    }
                }
            }
        } else {
            DataStore.setState("Walking to " + DataStore.getTreeType().getName() + " trees");
            DataStore.setBankArea(null);
            Area area = DataStore.getTreeArea();
            int length = area.getTiles().size();
            Position walk = area.getTiles().get(Random.nextInt(0, length));
            Path path = Movement.buildPath(walk);
            //pathExecutor.setRandomizeAll(true);
            if (Movement.getDestination() != null
                    && isFinalTile(false)){
                Time.sleep(100);
            } else {
                if(path != null) path.walk(pathExecutor);
            }
        }
        if(Settings.getSpeed() == Speed.AFK || Settings.getSpeed() == Speed.SLOW){
            return Random.nextInt(350, 600);
        }
        return Settings.getLoopSpeed();
    }

    private boolean bank(){
        //Log.info("Traverse[Bank]: " + isBanking()
        //+ " & " + Inventory.isFull()
        //+ " & " + !UserPref.getBankLocation().getPosition().equals(Players.getLocal().getPosition()));
        return  Settings.isBanking()
                && Inventory.isFull()
                && DataStore.getClosestBank() != null
                && Distance.between(DataStore.getClosestBank().getPosition(), Players.getLocal()) >= 15;
    }

    private boolean trees(){
        //Log.info("Traverse[Trees]: " + !Location.VARROCK_WEST.getArea().contains(Players.getLocal())
        //+ " & " + !Inventory.contains(PREDICATE_LOGS));
        //return !Location.VARROCK_WEST.getArea().contains(Players.getLocal())
        //       && !Inventory.contains(PREDICATE_LOGS);
        return !DataStore.getTreeArea().contains(Players.getLocal())
                && !Inventory.isFull();
    }

    private boolean isFinalTile(boolean banking){
        Position dest = Movement.getDestination();
        List<Position> tiles;
        tiles = banking ? DataStore.getBankArea().getTiles() : DataStore.getTreeArea().getTiles();
        for(Position p : tiles){
            if(p.equals(dest)){
                return true;
            }
        }
        return false;
    }

    private boolean checkDist(){
        if(Movement.getDestination() != null){
            Position dest = Movement.getDestination();
            int max = Random.nextInt(9,13);
            double dist = Distance.between(Players.getLocal(), dest);
            return dist < max;
        }
        return true;
    }
}
