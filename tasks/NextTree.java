package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Woodcutter;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;

public class NextTree implements Task {

    private final String CUT = "Chop down";

    @Override
    public boolean trigger() {
        //Log.info("" +
        //        Woodcutter.isCutting()
        //        + " OR ("
        //        + (!Inventory.isFull()
        //        + " & "
        //        + UserPref.getTreeArea().contains(Players.getLocal())
        //        + " & "
        //        + Players.getLocal().isMoving()));
        return DataStore.isStart()
                && (Woodcutter.isCutting()
                || (!Inventory.isFull()
        && DataStore.getTreeArea().contains(Players.getLocal())
        && Players.getLocal().isMoving()));
    }

    @Override
    public int execute() {
        DataStore.setState("Vigilance");
        if(!Woodcutter.isValid(DataStore.getCurrentTree())){
            if(Woodcutter.isValid(DataStore.getNextTree())){
                DataStore.setCurrentTree(DataStore.getNextTree());
                if(DataStore.getCurrentTree() != null && DataStore.getCurrentTree().interact(CUT)){
                    Time.sleepUntil(() -> Players.getLocal().isAnimating(), 50, 500);
                }
            }
        }
        DataStore.setNextTree(Woodcutter.findNextTree());
        return Settings.getLoopSpeed();
    }
}
