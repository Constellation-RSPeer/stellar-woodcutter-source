package StellarWoodcutter.tasks;

import StellarWoodcutter.framework.Woodcutter;
import StellarWoodcutter.framework.Task;
import StellarWoodcutter.res.DataStore;
import StellarWoodcutter.res.Settings;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;

import java.util.function.Predicate;

public class Cut implements Task {

    private final String CUT = "Chop down";
    private final String LOGS = "Logs";
    private final Predicate<Item> PREDICATE_LOGS = i -> i.getName().equals(LOGS)
            || i.getName().contains(LOGS.toLowerCase());

    @Override
    public boolean trigger() {
        return DataStore.isStart() && cut();
    }

    @Override
    public int execute() {
        DataStore.setState("Cutting " + DataStore.getTreeType().getName() + " tree");
        SceneObject tree = SceneObjects.getNearest(t -> t.getName().equals(DataStore.getTreeType().getName())
        && DataStore.getTreeArea().contains(t.getPosition()));
        if(tree != null){
            DataStore.setCurrentTree(tree);
            if(tree.interact(CUT)){
                Time.sleepUntil(() -> Players.getLocal().isAnimating(), 50, 2000);
            }
        }
        return Settings.getLoopSpeed();
    }

    private boolean cut(){
        //Log.info("Cut: " + !Woodcutter.isCutting()
        //+ " & " + !Players.getLocal().isMoving()
        //+ " & " + (SceneObjects.getNearest(UserPref.getTreeType().getName()) != null)
        //+ " & " + getDropScenario()
        //+ " & " + UserPref.getTreeArea().contains(Players.getLocal()));
        return  !Woodcutter.isCutting()
                && !Players.getLocal().isMoving()
                && SceneObjects.getNearest(DataStore.getTreeType().getName()) != null
                && getDropScenario()
                && DataStore.getTreeArea().contains(Players.getLocal());
    }

    private boolean getDropScenario(){
        switch (DataStore.getDropType().toString()){
            case "drop c1d1": return Inventory.getCount(PREDICATE_LOGS) < 1;
            case "drop c2d2": return Inventory.getCount(PREDICATE_LOGS) < 2;
            default: return !Inventory.isFull();
        }
    }
}
