package StellarWoodcutter.res;


import StellarWoodcutter.framework.DropSettings;
import StellarWoodcutter.framework.Tree;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.movement.position.Area;


public class DataStore {

    private static SceneObject currentTree;
    private static Tree treeType;
    private static DropSettings dropType;
    private static SceneObject nextTree;
    private static boolean start = false;
    private static Area bankArea;
    private static Area treeArea;
    private static int randomRun;
    private static boolean woodcuttingGuild = false;
    private static boolean usingSpecialAxe = false;
    private static BankLocation closestBank;
    private static String state;
    private static boolean dismissRandoms = false;

    /* DEPRECATED
    private static BankLocation bankLocation;
    private static Tree nextType;
    */

    public static BankLocation getClosestBank() { return closestBank; }

    public static void setClosestBank(BankLocation closestBank) { DataStore.closestBank = closestBank; }

    public static boolean isUsingSpecialAxe() { return usingSpecialAxe; }

    public static void setUsingSpecialAxe(boolean isUsingSpecialAxe) { DataStore.usingSpecialAxe = isUsingSpecialAxe; }

    public static boolean isWoodcuttingGuild() { return woodcuttingGuild; }

    public static void setWoodcuttingGuild(boolean woodcuttingGuild) { DataStore.woodcuttingGuild = woodcuttingGuild; }

    public static SceneObject getCurrentTree() { return currentTree; }

    public static void setCurrentTree(SceneObject currentTree) { DataStore.currentTree = currentTree; }

    public static Tree getTreeType() { return treeType; }

    public static void setTreeType(Tree treeType) { DataStore.treeType = treeType; }

    public static DropSettings getDropType() { return dropType; }

    public static void setDropType(DropSettings dropType) { DataStore.dropType = dropType; }

    public static SceneObject getNextTree() { return nextTree; }

    public static void setNextTree(SceneObject nextTree) { DataStore.nextTree = nextTree; }

    public static boolean isStart() { return start; }

    public static void setStart(boolean start) { DataStore.start = start; }

    public static Area getBankArea() { return bankArea; }

    public static void setBankArea(Area bankArea) { DataStore.bankArea = bankArea; }

    public static String getState() { return state; }

    public static void setState(String state) { DataStore.state = state; }

    public static Area getTreeArea() { return treeArea; }

    public static void setTreeArea(Area treeArea) { DataStore.treeArea = treeArea; }

    public static int getRandomRun() { return randomRun; }

    public static void setRandomRun(int randomRun) { DataStore.randomRun = randomRun; }

}
