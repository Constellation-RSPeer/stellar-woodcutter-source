package StellarWoodcutter.res;

import StellarWoodcutter.framework.Speed;
import org.rspeer.runetek.api.commons.math.Random;

public class Settings {

    private static boolean banking = false;
    private static Speed speed;

    public static void setSpeed(Speed speed) { Settings.speed = speed; }
    public static Speed getSpeed() { return speed; }

    public static boolean isBanking() { return banking; }

    public static void setBanking(boolean banking) { Settings.banking = banking; }

    public static int getLoopSpeed(){
        if(speed != null){
            switch(speed){
                case AFK: return Random.nextInt(8000, 20000);
                case SLOW: return Random.nextInt(3500, 7000);
                case MEDIUM: return Random.nextInt(1000, 2000);
                case NORMAL: return Random.nextInt(250, 450);
                case FAST: return Random.nextInt(50, 100);
            }
        }
        return Random.nextInt(250, 450);
    }

}
