package StellarWoodcutter.settings;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.rspeer.script.Script;
import org.rspeer.ui.Log;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PreferencesLoader {

    public static final Type STYPE = new TypeToken<UserPref>() {
    }.getType();

    private static String getSavePath(){
        Path path = Script.getDataDirectory();
        String folder = "StellarScripts";
        String bfFolder = "Woodcutter";
        Path finalPath = Paths.get(path.toString(), folder, bfFolder);
        return finalPath.toString();
    }

    public static void saveJson(UserPref settings){
        try (Writer writer = new FileWriter(getSavePath() + "//" + "settings.json")) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(settings, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static UserPref loadJson(Type type){
        try (Reader reader = new FileReader(getSavePath() + "//" + "settings.json")) {
            Gson gson = new GsonBuilder().create();
            return gson.fromJson(reader,type);
        }catch (Exception e) {
            Log.info("Exception");
            return null;
        }
    }

    public static void createFolderIfNotExists(){
        String savePath = getSavePath();
        Path path = Paths.get(savePath);
        if(!Files.exists(path)){
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
