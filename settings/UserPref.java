package StellarWoodcutter.settings;

import com.allatori.annotations.DoNotRename;

@DoNotRename
public class UserPref {

    @DoNotRename private int muleWorld;
    @DoNotRename private String muleName;
    @DoNotRename private String muleLocation;

    public UserPref(int muleWorld, String muleName, String muleLocation){
        this.muleWorld = muleWorld;
        this.muleName = muleName;
        this.muleLocation = muleLocation;
    }

    public int getMuleWorld() { return muleWorld; }
    public String getMuleName() { return muleName; }
    public String getMuleLocation() { return muleLocation; }
}
